declare module "http-core/http-client" {
    import { AxiosInstance, AxiosResponse, AxiosRequestConfig } from "axios";
    /**
     * 不自己实现http请求工具，采用对Axios二次封装的思路，只暴露常用方法
     * @class
     */
    export default class HttpClient {
        /**
         * axios实例
         * @private
         * @property
         */
        private axiosInstance;
        /**
         * 构造函数
         * @constructor
         * @param config AxiosRequestConfig
         */
        constructor(config?: AxiosRequestConfig);
        /**
         * 获取核心实例
         * @public
         */
        getCoreInstance(): AxiosInstance;
        /**
         * 发送一个请求
         * @param config AxiosRequestConfig
         */
        request<T = unknown, R = AxiosResponse<T>, D = any>(config: AxiosRequestConfig<D>): Promise<R>;
        /**
         * 发出一个get请求
         * @public
         * @param url string
         * @param config AxiosRequestConfig
         */
        get<T = unknown, R = AxiosResponse<T>, D = any>(url: string, config?: AxiosRequestConfig<D>): Promise<R>;
        /**
         * 发出一个post请求
         * @public
         * @param url string
         * @param config AxiosRequestConfig
         */
        post<T = unknown, R = AxiosResponse<T>, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>): Promise<R>;
        /**
         * 发出一个put请求
         * @public
         * @param url string
         * @param config AxiosRequestConfig
         */
        put<T = unknown, R = AxiosResponse<T>, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>): Promise<R>;
    }
}
declare module "http-core/index" {
    import HttpClient from "http-core/http-client";
    export { HttpClient };
}
declare module "index" {
    export * from "http-core/index";
}
