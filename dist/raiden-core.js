define("http-core/http-client", ["require", "exports", "axios"], function (require, exports, axios_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * 不自己实现http请求工具，采用对Axios二次封装的思路，只暴露常用方法
     * @class
     */
    var HttpClient = /** @class */ (function () {
        /**
         * 构造函数
         * @constructor
         * @param config AxiosRequestConfig
         */
        function HttpClient(config) {
            this.axiosInstance = axios_1.default.create(config);
        }
        /**
         * 获取核心实例
         * @public
         */
        HttpClient.prototype.getCoreInstance = function () {
            return this.axiosInstance;
        };
        /**
         * 发送一个请求
         * @param config AxiosRequestConfig
         */
        HttpClient.prototype.request = function (config) {
            return this.axiosInstance.request(config);
        };
        /**
         * 发出一个get请求
         * @public
         * @param url string
         * @param config AxiosRequestConfig
         */
        HttpClient.prototype.get = function (url, config) {
            return this.axiosInstance.get(url, config);
        };
        /**
         * 发出一个post请求
         * @public
         * @param url string
         * @param config AxiosRequestConfig
         */
        HttpClient.prototype.post = function (url, data, config) {
            return this.axiosInstance.post(url, config);
        };
        /**
         * 发出一个put请求
         * @public
         * @param url string
         * @param config AxiosRequestConfig
         */
        HttpClient.prototype.put = function (url, data, config) {
            return this.axiosInstance.put(url, config);
        };
        return HttpClient;
    }());
    exports.default = HttpClient;
});
define("http-core/index", ["require", "exports", "http-core/http-client"], function (require, exports, http_client_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.HttpClient = http_client_1.default;
});
define("index", ["require", "exports", "http-core/index"], function (require, exports, http_core_1) {
    "use strict";
    function __export(m) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
    Object.defineProperty(exports, "__esModule", { value: true });
    __export(http_core_1);
});
//# sourceMappingURL=raiden-core.js.map