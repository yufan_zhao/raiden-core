import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from "axios";

/**
 * 不自己实现http请求工具，采用对Axios二次封装的思路，只暴露常用方法
 * @class
 */
export default class HttpClient
{
    /**
     * axios实例
     * @private
     * @property
     */
    private axiosInstance: AxiosInstance;

    /**
     * 构造函数
     * @constructor
     * @param config AxiosRequestConfig
     */
    constructor(config?: AxiosRequestConfig)
    {
        this.axiosInstance = axios.create(config);
    }

    /**
     * 获取核心实例
     * @public
     */
    public getCoreInstance(): AxiosInstance
    {
        return this.axiosInstance;
    }

    /**
     * 发送一个请求
     * @param config AxiosRequestConfig
     */
    public request<T = unknown, R = AxiosResponse<T>, D = any>(config: AxiosRequestConfig<D>): Promise<R>
    {
        return this.axiosInstance.request(config);
    }

    /**
     * 发出一个get请求
     * @public
     * @param url string
     * @param config AxiosRequestConfig
     */
    public get<T = unknown, R = AxiosResponse<T>, D = any>(url: string, config?: AxiosRequestConfig<D>): Promise<R>
    {
        return this.axiosInstance.get(url, config);
    }

    /**
     * 发出一个post请求
     * @public
     * @param url string
     * @param config AxiosRequestConfig
     */
    public post<T = unknown, R = AxiosResponse<T>, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>): Promise<R>
    {
        return this.axiosInstance.post(url, config);
    }

    /**
     * 发出一个put请求
     * @public
     * @param url string
     * @param config AxiosRequestConfig
     */
    public put<T = unknown, R = AxiosResponse<T>, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>): Promise<R>
    {
        return this.axiosInstance.put(url, config);
    }
}
